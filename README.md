# .NET Custom Collections<T>
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
> Chaos Array Assignment
## Table of Contents
- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)
## Run
```
Visual Studio Run
```
## Components
### ChaosArray.cs
Custom Collection, compatible with any Type, methods to Add and GetItem.
### Custom Exception Handlers
ChaosArrayElementOccupiedException.cs, custom handler for ElementOccupiedException
ChaosArrayNullPointerException.cs, custom handler for NullPointerException
## Maintainers
[Christopher Toussi (@ctuss)](https://gitlab.com/ctuss)
## Contributing
PRs accepted.
Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.
## License
Experis © 2020 Noroff Accelerate AS