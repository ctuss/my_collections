﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_Collections
{
    class ChaosArray<T>
    {
        public T[] ca;
        int elements, rnd;

        Random random;

        public ChaosArray()
        {
            ca = new T[10];
            elements = 0;
            random = new Random();
        }

        public void Add(T t)
        {
            if (elements >= ca.Length)
            {
                ExtendArray();
            }

            rnd = random.Next(0, 10);

            if (ca[rnd] == null)
            {
                ca[rnd] = t;
                elements++;
            }
            else
            {
                throw new ChaosArrayElementOccupiedException("Element occupied");
            }
        }

        public T GetItem()
        {
            rnd = random.Next(0, ca.Length);
            if (ca[rnd] == null)
            {
                throw new ChaosArrayNullPointerException();
            }
            else
            {
                return ca[rnd];
            }
        }

        public void ExtendArray()
        {
            T[] copyOfArray = new T[ca.Length];
            ca.CopyTo(copyOfArray, 0);
            ca = new T[copyOfArray.Length * 2];

            foreach (T elem in copyOfArray)
            {
                Add(elem);
            }
        }
    }
}
