﻿using System;

namespace My_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            ChaosArray<string> myArray = new ChaosArray<string>();
            string[] elements = new string[] { "Hallo", "Hei", "HeiHei" };

            for (int i = 0; i < elements.Length; i++)
            {
                try
                {
                    myArray.Add(elements[i]);
                }
                catch (ChaosArrayElementOccupiedException)
                {
                    Console.WriteLine("Did not find empty position.");
                }
            }

            try
            {
                myArray.GetItem();
            }
            catch (ChaosArrayNullPointerException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void AddThingToArray(string element)
        {
            try
            {
                
            }
            catch (ChaosArrayElementOccupiedException)
            {

            }
        }
    }
}
