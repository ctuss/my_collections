﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_Collections
{
    class ChaosArrayElementOccupiedException : Exception
    {
        public ChaosArrayElementOccupiedException()
        {

        }

        public ChaosArrayElementOccupiedException(string message) : base(message)
        {

        }

        public ChaosArrayElementOccupiedException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
