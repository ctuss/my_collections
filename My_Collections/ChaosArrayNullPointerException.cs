﻿using System;
using System.Collections.Generic;
using System.Text;

namespace My_Collections
{
    class ChaosArrayNullPointerException : Exception
    {
        public ChaosArrayNullPointerException()
        {

        }

        public ChaosArrayNullPointerException(string message) : base(message)
        {

        }

        public ChaosArrayNullPointerException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
